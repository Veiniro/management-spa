import Vue from 'vue';

// Components
import VModal from 'vue-js-modal';
import Notifications from 'vue-notification';
import velocity from 'velocity-animate';
import Multiselect from 'vue-multiselect';
import Vuelidate from 'vuelidate';
import AnimatedVue from 'animated-vue';
import App from './App.vue';
import Icon from './components/misc/Icon.vue';
import Button from './components/misc/Button.vue';
import Loader from './components/misc/Loader.vue';
import router from './router';
import store from './vuex';

// Styles
import 'normalize.css/normalize.css';
import 'font-awesome/css/font-awesome.min.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import 'animate.css/animate.css';
import 'vue-loaders/dist/vue-loaders.css';
import VueLoaders from 'vue-loaders';

Vue.use(AnimatedVue);
Vue.use(VModal, { dynamic: true, injectModalsContainer: true, dialog: true });
Vue.use(Notifications, { velocity });
Vue.use(Vuelidate);
Vue.use(VueLoaders);
Vue.component('user-icon', Icon);
Vue.component('user-button', Button);
Vue.component('user-loader', Loader);
Vue.component('multiselect', Multiselect);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
