#Management SPA

#### Environment
***
* PHP 7.2
* Lavarel 5.8
* Vue.js 2.6
* Apache 2.4
* MySQL 8


#### Setup
***
1. Clone this repository ```git clone https://bitbucket.org/Veiniro/management-spa/```.
2. Run composer packages installation ```composer install```.
3. Run frontend build moving to frontend folder and execute ```npm run serve```.
4. Rename ```.env.example``` to ```.env``` and change your db params. 
5. Run db migration ```php artisan migrate``` and seeding ```php arrisan db:seed```. 
6. Run the web-server by ```php artisan serve``` 
