<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        factory(App\Department::class,10)->create();
    }
}
