<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        factory(App\Employee::class,20)->create();
    }
}
