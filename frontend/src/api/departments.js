
import axios from 'axios';

export async function getDepartments() {
  const response = await axios.get('http://localhost:8000/api/departments');
  const departments = response.data.data;
  return departments;
}

export async function createDepartment(department) {
  const response = await axios.post('http://localhost:8000/api/departments', department);
  const newDepartment = response.data.data;
  return newDepartment;
}

export async function updateDepartment(department) {
  const response = await axios.put(`http://localhost:8000/api/departments/${department.id}`, department);
  const newDepartment = response.data.data;
  return newDepartment;
}

export async function deleteDepartment(id) {
   await axios.delete(`http://localhost:8000/api/departments/${id}`);
   return id;
}
