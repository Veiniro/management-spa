<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $timestamps = false;

    protected $fillable = ['title'];
    protected $hidden = ['pivot'];
    public static $rules = [
        'title' => 'required|min:5|max:100'
    ];

    public function employees()
    {
        return $this
            ->belongsToMany(
                Employee::class,
                'departments_employees',
                'department_id',
                'employee_id'
            );
    }
}
