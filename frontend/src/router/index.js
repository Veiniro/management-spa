import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

function dynamicPage(route) {
  return { page: route.params.page ? +route.params.page : 1 };
}

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'BasePage',
      path: '/',
      component: () => import('../pages/BasePage'),
      redirect: {
        name: 'Dashboard',
      },
      children: [
        {
          name: 'Dashboard',
          path: 'dashboard',
          component: () => import('../pages/Dashboard/Dashboard.vue'),
        },
        {
          name: 'Departments',
          path: 'departments/:page(\\d+)?',
          props: dynamicPage,
          component: () => import('../pages/Departments/Departments.vue'),
        },
        {
          name: 'Employees',
          path: 'employees/:page(\\d+)?',
          props: dynamicPage,
          component: () => import('../pages/Employees/Employees.vue'),
        },
      ],
    },
    {
      path: '*',
      redirect: {
        name: 'Dashboard',
      },
    },
  ],
});
