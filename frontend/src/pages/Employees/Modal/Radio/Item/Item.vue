<template>
  <div class="radio__item" @click="select">
    <div class="radio__item-circle" :class="{'radio__item-circle_active':active}"></div>
    <span class="radio__item-title">{{item.text}}</span>
  </div>
</template>
<script>
  export default {
    props: {
      item: Object,
      active: Boolean,
    },

    methods: {
      select() {
        this.$emit('change', this.item.value);
      },
    },
  };
</script>
<style lang="scss">
  .radio__item{
    display: flex;
    align-items: center;
    margin-right: 5px;
    cursor: pointer;
    &:last-of-type{
      margin-right: 0;
    }
  }
  .radio__item-circle{
    margin-right: 5px;
    width: 30px;
    height: 30px;
    display: block;
    border-radius: 100%;
    background-color: $primary;
    position: relative;
    &:before{
      content: "";
      position: absolute;
      width: 15px;
      height: 15px;
      border-radius: 100%;
      border: 1px solid $white;
      background-color: $white;
      top:50%;
      left: 50%;
      transform: translate(-50%,-50%);
    }
    &_active{
      &:before{
        border: 3px solid $white;
        background-color: $primary;
      }
    }
  }
  .radio__item-title{
    font-family: "Roboto";
    font-weight: 300;
  }
</style>
