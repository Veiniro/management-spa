<?php

use Illuminate\Database\Seeder;

class DepartmentsEmployeesSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $departments = App\Department::all();
        App\Employee::all()->each(function (App\Employee $e) use ($departments) {
            $e->departments()->attach($departments->random(rand(1, 3))->pluck('id')->toArray());
        });
    }
}
