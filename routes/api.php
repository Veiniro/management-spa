<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/departments', 'DepartmentsController@index');
Route::get('/departments/{id}', 'DepartmentsController@show');
Route::post('/departments', 'DepartmentsController@create');
Route::put('/departments/{id}', 'DepartmentsController@update');
Route::delete('/departments/{id}', 'DepartmentsController@destroy');

Route::get('/employees', 'EmployeesController@index');
Route::get('/employees/{id}', 'EmployeesController@show');
Route::post('/employees', 'EmployeesController@create');
Route::put('/employees/{id}', 'EmployeesController@update');
Route::delete('/employees/{id}', 'EmployeesController@destroy');
