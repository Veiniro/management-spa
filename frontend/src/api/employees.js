
import axios from 'axios';

export async function getEmployees() {
  const response = await axios.get('http://localhost:8000/api/employees');
  const employees = response.data.data;
  return employees;
}

export async function createEmployee(employee) {
  const response = await axios.post('http://localhost:8000/api/employees', employee);
  const newEmployee = response.data.data;
  return newEmployee;
}

export async function updateEmployee(employee) {
  const response = await axios.put(`http://localhost:8000/api/employees/${employee.id}`, employee);
  const newEmployee = response.data.data;
  return newEmployee;
}

export async function deleteEmployee(id) {
   await axios.delete(`http://localhost:8000/api/employees/${id}`);
   return id;
}
