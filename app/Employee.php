<?php

namespace App;

use App\Department;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $timestamps = false;

    protected $fillable = ['firstName', 'lastName', 'surName', 'sex', 'salary'];
    protected $hidden = ['pivot'];

    public static $rules = [
        'firstName' => 'required|min:3|max:100',
        'lastName' => 'required|min:3|max:100',
        'surName' => 'min:3|max:100',
        'sex' => 'in:male,female',
        'salary' => 'between:1000,5000|numeric',
        'departments' => 'required|array|min:1'
    ];
    public function departments()
    {
        return $this
            ->belongsToMany(
                Department::class,
                'departments_employees',
                'employee_id',
                'department_id'
            );
    }
}
