import Vue from 'vue';
import Vuex from 'vuex';
import DepartmentsModule from './departments';
import EmployeesModule from './employees';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    DepartmentsModule,
    EmployeesModule,
  },
});
