import {
  createDepartment,
  deleteDepartment,
  getDepartments,
  updateDepartment,
} from '../api/departments';

export default {
  state: {
    departments: [],
  },
  mutations: {
    SET_DEPARTMENTS(state, departments) {
      state.departments = departments;
    },
    PUSH_DEPARTMENT(state, department) {
      state.departments.push(department);
    },
    UPDATE_DEPARTMENT(state, department) {
      const departmentItem = state.departments.find(item => item.id === department.id);
      const departmentIndex = state.departments.indexOf(departmentItem);
      state.departments.splice(departmentIndex, 1, department);
    },

    REMOVE_DEPARTMENT(state, id) {
      const departmentItem = state.departments.find(item => item.id === id);
      const departmentIndex = state.departments.indexOf(departmentItem);
      state.departments.splice(departmentIndex, 1);
    },
  },
  actions: {
    async GET_DEPARTMENTS({ commit }) {
      commit('SET_DEPARTMENTS', await getDepartments());
    },

    async CREATE_DEPARTMENT({ commit }, department) {
      commit('PUSH_DEPARTMENT', await createDepartment(department));
    },

    async EDIT_DEPARTMENT({ commit }, department) {
      commit('UPDATE_DEPARTMENT', await updateDepartment(department));
    },

    async DELETE_DEPARTMENT({ commit }, id) {
      commit('REMOVE_DEPARTMENT', await deleteDepartment(id));
    },
  },
  getters: {
    departments: state => state.departments,
  },
};
