<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Http\Resources\Department as DepartmentResource;


class DepartmentsController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $departments = Department::with('employees')->get();
        return DepartmentResource::collection($departments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return DepartmentResource
     */
    public function create(Request $request)
    {
        return new DepartmentResource(Department::create($request->validate(Department::$rules)));
    }

    /**
     * @param int $id
     * @return DepartmentResource
     */
    public function show($id)
    {
        return new DepartmentResource($id);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return DepartmentResource
     */
    public function update(Request $request, $id)
    {
        $department = Department::findOrFail($id);
        $department->update($request->validate(Department::$rules));

        return new DepartmentResource($department);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $department = Department::findOrFail($id);
        $department->delete();

        return \response()->json(null, 204);
    }
}
