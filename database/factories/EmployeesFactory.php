<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'firstName' => $faker->firstName(),
        'lastName' => $faker->lastName(),
        'surName' => $faker->firstName(),
        'sex' => $faker->randomElement(["male", "female"]),
        'salary' => $faker->numberBetween(1000,3000),
    ];
});
