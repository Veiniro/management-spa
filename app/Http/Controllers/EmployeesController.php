<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Department;
use App\Http\Resources\Employee as EmployeeResource;

class EmployeesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $departments = Employee::all();
        return EmployeeResource::collection($departments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return EmployeeResource
     */
    public function create(Request $request)
    {
        $employee = Employee::create($request->validate(Employee::$rules));

        $departments = Department::find(($request->all())['departments']);
        $employee->departments()->sync($departments);

        return new EmployeeResource($employee);
    }

    /**
     *
     * @param int $id
     * @return EmployeeResource
     */
    public function show($id)
    {
        return new EmployeeResource(Employee::find($id));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return EmployeeResource
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $employee->update($request->all());

        $departments = Department::find(($request->all())['departments']);
        $employee->departments()->sync($departments);

        return new EmployeeResource($employee);
    }

    /**
     * @param int $id
     * @return int
     */
    public function destroy($id)
    {
        $department = Employee::findOrFail($id);
        $department->delete();

        return 204;
    }
}
