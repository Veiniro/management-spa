import {
  createEmployee,
  deleteEmployee,
  getEmployees,
  updateEmployee,
} from '../api/employees';

export default {
  state: {
    employees: [],
  },
  mutations: {
    SET_EMPLOYEES(state, employees) {
      state.employees = employees;
    },
    PUSH_EMPLOYEE(state, employee) {
      state.employees.push(employee);
    },
    UPDATE_EMPLOYEE(state, employee) {
      const employeeItem = state.employees.find(item => item.id === employee.id);
      const employeeIndex = state.employees.indexOf(employeeItem);
      state.employees.splice(employeeIndex, 1, employee);
    },

    REMOVE_EMPLOYEE(state, id) {
      const employeeItem = state.employees.find(item => item.id === id);
      const employeeIndex = state.employees.indexOf(employeeItem);
      state.employees.splice(employeeIndex, 1);
    },
  },
  actions: {
    async GET_EMPLOYEES({ commit }) {
      commit('SET_EMPLOYEES', await getEmployees());
    },

    async CREATE_EMPLOYEE({ commit }, employee) {
      commit('PUSH_EMPLOYEE', await createEmployee(employee));
    },

    async EDIT_EMPLOYEE({ commit }, employee) {
      commit('UPDATE_EMPLOYEE', await updateEmployee(employee));
    },

    async DELETE_EMPLOYEE({ commit }, id) {
      commit('REMOVE_EMPLOYEE', await deleteEmployee(id));
    },
  },
  getters: {
    employees: state => state.employees,
  },
};
